-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 28 oct. 2022 à 01:30
-- Version du serveur : 10.4.24-MariaDB
-- Version de PHP : 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `selniam`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `id_users` int(11) NOT NULL,
  `nom_users` text NOT NULL,
  `password_users` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`id_users`, `nom_users`, `password_users`) VALUES
(1, 'Pierre', '0a331ed73ab2e918040c8e245bafc758c7656c1d84de25be1dc6b67a9d41cd26');

-- --------------------------------------------------------

--
-- Structure de la table `bulletin`
--

CREATE TABLE `bulletin` (
  `NumBulletin` int(11) NOT NULL,
  `TauxBulletin` int(11) NOT NULL,
  `DateBulletin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `bulletin`
--

INSERT INTO `bulletin` (`NumBulletin`, `TauxBulletin`, `DateBulletin`) VALUES
(1015073672, 1500, '2022-10-27'),
(1030504132, 2500, '2022-10-27'),
(1050011399, 1000, '2022-10-27'),
(1018168445, 1200, '2022-10-27');

-- --------------------------------------------------------

--
-- Structure de la table `comptable`
--

CREATE TABLE `comptable` (
  `NumComp` int(11) NOT NULL,
  `NomComp` text NOT NULL,
  `PrenComp` text NOT NULL,
  `AdrComp` text NOT NULL,
  `TelComp` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `comptable`
--

INSERT INTO `comptable` (`NumComp`, `NomComp`, `PrenComp`, `AdrComp`, `TelComp`) VALUES
(2147483647, 'Kra', 'James Evra', 'Abidjan', '0978373633');

-- --------------------------------------------------------

--
-- Structure de la table `contrat`
--

CREATE TABLE `contrat` (
  `NumCont` int(11) NOT NULL,
  `LibCont` text NOT NULL,
  `DatCont` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `contrat`
--

INSERT INTO `contrat` (`NumCont`, `LibCont`, `DatCont`) VALUES
(325697794, 'Contrat à duré détermier(CDD)', '2022-10-27'),
(644355323, 'Contrat à duré détermier(CDI)', '2022-10-27'),
(1207327159, 'Contrat à duré détermier(CDD)', '2022-10-27'),
(1480392632, 'Contrat à duré détermier(CDD)', '2022-10-27');

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

CREATE TABLE `employe` (
  `id_emploi` int(11) NOT NULL,
  `Matricule_emploi` text NOT NULL,
  `nom_emploi` text NOT NULL,
  `Prenom_emploi` text NOT NULL,
  `tel_emploi` varchar(10) NOT NULL,
  `Num_equip_emploi` text NOT NULL,
  `DtNs_emploi` date NOT NULL,
  `Addr_emploi` text NOT NULL,
  `NumBulletin` int(11) NOT NULL,
  `Contrats_emploi` text NOT NULL,
  `NumPoste` int(11) NOT NULL,
  `NumCont` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `employe`
--

INSERT INTO `employe` (`id_emploi`, `Matricule_emploi`, `nom_emploi`, `Prenom_emploi`, `tel_emploi`, `Num_equip_emploi`, `DtNs_emploi`, `Addr_emploi`, `NumBulletin`, `Contrats_emploi`, `NumPoste`, `NumCont`) VALUES
(1, '12323443', 'Kra', 'James Evra', '0978373633', '2456', '2000-03-12', 'Abidjan', 1015073672, 'Contrat à duré détermier(CDD)', 787840347, 325697794),
(2, '211345', 'Toure', 'Woagnienlin Axel', '0757346565', '0001', '1998-04-03', 'Abidjan', 1030504132, 'Contrat à duré détermier(CDD)', 1357707731, 1207327159),
(3, '10003443', 'Koffi', 'Pierre Franck', '0978000633', '2456', '2000-04-12', 'Abidjan', 1050011399, 'Contrat à duré détermier(CDI)', 1507069765, 644355323),
(4, '12343', 'Kouassi', 'James Evra', '0970073600', '2456', '1998-04-12', 'Abidjan', 1018168445, 'Contrat à duré détermier(CDD)', 1565846332, 1480392632);

-- --------------------------------------------------------

--
-- Structure de la table `gerant`
--

CREATE TABLE `gerant` (
  `NumGerant` int(11) NOT NULL,
  `NomGerant` text NOT NULL,
  `PrenGerant` text NOT NULL,
  `AdrGerant` text NOT NULL,
  `TelGerant` int(11) NOT NULL,
  `NumPoste` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `gerant`
--

INSERT INTO `gerant` (`NumGerant`, `NomGerant`, `PrenGerant`, `AdrGerant`, `TelGerant`, `NumPoste`) VALUES
(409818311, 'Koffi', 'Pierre Franck', 'Abidjan', 978000633, 1507069765);

-- --------------------------------------------------------

--
-- Structure de la table `poste`
--

CREATE TABLE `poste` (
  `NumPoste` int(11) NOT NULL,
  `LibPoste` text NOT NULL,
  `DateAff` date NOT NULL,
  `HeureDebut` time NOT NULL,
  `HeureFin` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `poste`
--

INSERT INTO `poste` (`NumPoste`, `LibPoste`, `DateAff`, `HeureDebut`, `HeureFin`) VALUES
(787840347, 'Comptable', '2022-10-27', '00:00:00', '00:00:00'),
(1357707731, 'Directeur Génerale', '2022-10-27', '00:00:00', '00:00:00'),
(1507069765, 'Gerant', '2022-10-27', '00:00:00', '00:00:00'),
(1565846332, 'Ouvrier', '2022-10-27', '00:00:00', '00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `postes`
--

CREATE TABLE `postes` (
  `id_postes` int(11) NOT NULL,
  `nom_postes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `postes`
--

INSERT INTO `postes` (`id_postes`, `nom_postes`) VALUES
(1, 'GERANT'),
(2, 'COMPTABLE'),
(3, 'EMPLOYE');

-- --------------------------------------------------------

--
-- Structure de la table `rendre_compte`
--

CREATE TABLE `rendre_compte` (
  `DateRapport` date NOT NULL,
  `NumComp` int(11) NOT NULL,
  `NumGerant` int(11) NOT NULL,
  `SalairJours` int(11) NOT NULL,
  `Matricule_emploi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `rendre_compte`
--

INSERT INTO `rendre_compte` (`DateRapport`, `NumComp`, `NumGerant`, `SalairJours`, `Matricule_emploi`) VALUES
('2022-10-27', 1298, 910986657, 33000, 12323443),
('2022-10-27', 29285, 910986657, 45000, 211345);

-- --------------------------------------------------------

--
-- Structure de la table `suivre`
--

CREATE TABLE `suivre` (
  `DateSuivi` date NOT NULL,
  `NumPoste` int(11) NOT NULL,
  `NumGerant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `suivre`
--

INSERT INTO `suivre` (`DateSuivi`, `NumPoste`, `NumGerant`) VALUES
('2022-10-27', 12323443, 910986657),
('2022-10-27', 211345, 910986657);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_users`);

--
-- Index pour la table `comptable`
--
ALTER TABLE `comptable`
  ADD PRIMARY KEY (`NumComp`);

--
-- Index pour la table `contrat`
--
ALTER TABLE `contrat`
  ADD PRIMARY KEY (`NumCont`);

--
-- Index pour la table `employe`
--
ALTER TABLE `employe`
  ADD PRIMARY KEY (`id_emploi`);

--
-- Index pour la table `postes`
--
ALTER TABLE `postes`
  ADD PRIMARY KEY (`id_postes`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_users` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `contrat`
--
ALTER TABLE `contrat`
  MODIFY `NumCont` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1480392633;

--
-- AUTO_INCREMENT pour la table `employe`
--
ALTER TABLE `employe`
  MODIFY `id_emploi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `postes`
--
ALTER TABLE `postes`
  MODIFY `id_postes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
