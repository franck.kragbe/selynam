<?php
// existance du click sur la boutont Valider
if ( isset( $_POST['Valider'] ) ) {
    //existance de la valeur du champ Libélé de Poste
    if ( isset( $_POST['Matricule'] ) && !empty( $_POST['Matricule'] ) ) {
        //recuperation de la valeur du champ Matricule
        $Matricule = $_POST['Matricule'];
            if ( isset( $_POST['HeureDebut'] ) && !empty( $_POST['HeureDebut'] ) ) {
                $HeureDebut = $_POST['HeureDebut'];
                if ( isset( $_POST['HeureFin'] ) && !empty( $_POST['HeureFin'] ) ) {
                    $HeureFin = $_POST['HeureFin'];
                    //verifier heure l'heure de fin doit etre toujours inferieure à de heure débat
                    //verifie les heures en convertissant en timestamp https://www.php.net/manual/en/function.time.php
                    $HF =(int)strtotime($HeureFin);
                    $HD =(int)strtotime($HeureDebut);
                    //compare les timestamps et le timestamp de heure de fin soit supperieur en timestamp du heure de debat
                    if($HD<$HF){
                    function securite( $donnee ) {
                    //On affecte les valeures du resultat des functions dans la variables de departs
                        $donnee = trim( $donnee ); //supprime les caractères invisibles en début et fin de chaîne espaces https://www.php.net/manual/fr/function.trim.php
                        $donnee = stripslashes( $donnee );//Retourne une chaîne dont les antislashs on été supprimés. 
                        $donnee = strip_tags( $donnee );//supprimé tous les octets nuls, toutes les balises PHP et HTML du code. https://www.php.net/manual/fr/function.strip-tags
                        return $donnee;// retourne la varaible pourqu'on puisse  la récuperer hors function
                    }
                        $Matricule=securite($Matricule);
                        $HeureDebut=securite($HeureDebut);
                        $HeureFin=securite($HeureFin);
                        $DB=new DB();
                        //verifier si le poste est déjà enregistrer
                        $existes=$DB->db->prepare("SELECT * FROM employe WHERE Matricule_emploi =:Matricule_emploi");
                        $existes->bindParam(':Matricule_emploi',$Matricule);
						$existes->execute();
						$cont=$existes->rowCount();
                        if($cont==1){
							$existe=$existes->fetch();
							$existe=$existe['NumPoste'];
							$affecte=$DB->db->prepare("UPDATE poste SET HeureDebut=:HeureDebut WHERE NumPoste=:NumPoste");
							$affecte->bindParam(':HeureDebut',$HeureDebut);
							$affecte->bindParam(':NumPoste',$existe);
							$affecte->execute();
							$affecte=$DB->db->prepare("UPDATE poste SET HeureFin=:HeureFin WHERE NumPoste=:NumPoste");
							$affecte->bindParam(':HeureFin',$HeureFin);
							$affecte->bindParam(':NumPoste',$existe);
							$affecte->execute();
							
                        }else{
                            $error="Le numero ".$Matricule." n'existe pas";
                        }
                    }else{
                        $error="L'heure de fin est inferieur à l'heure de début";}
                }else{
                    $error="Le champ heure de fin est vide";}
            }else{
                $error="Le champ heure de debut est vide";}
    }else{
        $error="Le champ est libélé de poste est vide";
                }
            }
            ?>