<?php
//verification des valeurs saissient dans les inputs
if (isset($_POST['Connexion'])) { // detection si l'utilisateur a appuyer sur le bouton Connexion
    if (isset($_POST['Login']) && !empty($_POST['Login'])) { //verification si la valeur entré pour le champ login est existe et est non vide ('',' ')
        $login = $_POST['Login']; // recuperation de la valeur du champ login dans une variable $login
        if (strlen($login) > 4) { //verification si la taille de la variable login
            if (isset($_POST['Password']) && !empty($_POST['Password'])) { //verification si la valeur entré pour le champ Mot de passe est existe et est non vite ('',' ')
                $password = $_POST['Password'];
                //si toute les variables sont Ok 
                // on vas la securisation des valeure 
                //NB:: Ne faites jamais confiance aux donnée envoyer par les USER
                //creer une function de securité pour sterelisé les données
                function securite($donnee)
                {
                    //On affecte les valeures du resultat des functions dans la variables de departs
                    $donnee = trim($donnee); //supprime les caractères invisibles en début et fin de chaîne espaces https://www.php.net/manual/fr/function.trim.php
                    $donnee = stripslashes($donnee); //Retourne une chaîne dont les antislashs on été supprimés. 
                    $donnee = strip_tags($donnee); //supprimé tous les octets nuls, toutes les balises PHP et HTML du code. https://www.php.net/manual/fr/function.strip-tags
                    return $donnee; // retourne la varaible pourqu'on puisse  la récuperer hors function
                }
                //cuirsson des variables
                $login = securite($login);
                $password = securite($password);
                // objectif est d'empêche des injection SQL XSS ...
                //Hashage du mot de passe
                // ajout de sel à gauche avec le mot SEL et NIAM à droit + la valeur du login

                $password = base64_encode("SEL") . $password . md5('NIAM') . $login;
                $password = base64_encode($password);
                $password = hash('sha256', $password);
                //echo $password;
                //appel de la funtion
                $DB = new DB(); //on initialise la valeurs la function de classe config-db.class
                //Verification des donnée dans la base de donnée
                $check = $DB->db->prepare("SELECT * FROM admin WHERE nom_users=:nom_users AND password_users=:password_users"); // syntaxe SQL pour verifier si les correctes 😁😁
                //On fait appel a une funtion Native des php empêches les injection SQl
                $check->bindParam('nom_users', $login);
                $check->bindParam('password_users', $password);
                // on execute la fonction 
                $check->execute();
                //on compte le nombre de fois que l'information correct est trouver dans la table admin
                $error = $check->rowCount();
                //conditon sur le nombre de trouver
                if ($error == 1) {
                    //Login:Pierre
                    // Mote de passe :azertyuiop
                    //on fait une redirection sur la page d'accueil
                    //on pouvais dit si error different de 0 alors il y'a connexion mais ce cas peut cause une faille de duplications d'identite 
                    // c'est-à-dire que au moins deux users on les mêmes identifiants
                    header("location:admin/index.php?valide=ok");
                } else {
                    //on elimine tout les autre cas 
                    $error = "Désolé identifiants incorrects";
                }
            } else {
                //dans le cas contraire si le champ Mot de passe est vide est on retourne
                $error = 'Le champ Mot de passe est Vide';
            }
        } else {
            //dans le cas contrait si le champ login est inferieur à 4 on retourne
            $error = 'La longueur de la valeur de la champ login doit etre superier à 4 caractères';
        }
    } else {
        //dans le cas contrait si le champ login est vide est on retourne
        $error = 'Le champ login est Vide';
    }
}
