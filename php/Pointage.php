<?php
if ( isset( $_POST['Go'] ) ) {
    $Numero = ( int )$_POST['Numero'];
    if ( !empty( $Numero ) ) {
        $DB = new DB();
        $check_gerants = $DB->db->prepare( 'SELECT * FROM employe WHERE Num_equip_emploi=:Num_equip_emploi LIMIT 1' );
        $check_gerants->bindParam( ':Num_equip_emploi', $Numero );
        $check_gerants->execute();
        $check_gerant = $check_gerants->rowCount();
        if ( $check_gerant == 1 ) {
            $check_gerants = $check_gerants->fetch();
            $NumPoste = $check_gerants['NumPoste'];
            $Matricule_emploi = $check_gerants['Matricule_emploi'];
			$_SESSION['Matricule_emploi']=$Matricule_emploi;
			$_SESSION['NumPoste']=$check_gerants['NumPoste'];
            $check_post = $DB->db->prepare( "SELECT * FROM gerant WHERE NumPoste=:NumPoste" );
            $check_post->bindParam( ":NumPoste", $NumPoste );
            $check_post->execute();
            $check_post_exist=$check_post->rowCount();
            if ( $check_post_exist == 1 ) {
                $check_post = $check_post->fetch();
                $_SESSION["Numero"] = $Numero;
                $_SESSION["NumGerant"] = $check_post['NumGerant'];
                $_SESSION["Chef"] = $check_post['NomGerant'].' '.$check_post['PrenGerant'];
            } else {
                $error = "L'employé n'a pas le titre de gerant";
            }
        } else {
            $error = "Numero d'equipe n'existe pas";
        }
    } else {
        $error = 'Le champ Numero est vide';
    }
}
if ( isset( $_POST['Valider'] ) ) {
    $Nom = $_POST['Nom'];
    $heureffec = $_POST['heureffec'];
    $heureajouter = $_POST['heureajouter'];
    $heureretranc = $_POST['heureretranc'];
    $Matricule = $_POST['Matricule'];
    if ( !empty( $Nom ) ) {
        if ( !empty( $heureffec ) ) {
            $heureffec = ( int )$heureffec;
            if ( $heureffec >= 0 ) {
                if ( $heureajouter >= 0 ) {
                    if ( !empty( $Matricule ) ) {
                        $DB = new DB();
                        $MatExists = $DB->db->prepare( "SELECT Matricule_emploi,nom_emploi,Prenom_emploi,NumBulletin FROM employe WHERE Matricule_emploi =:Matricule_emploi" );
                        $MatExists->bindParam( ':Matricule_emploi', $Matricule );
                        $MatExists->execute();
                        $MatExist = $MatExists->rowCount();
                        $MatExists = $MatExists->fetch();
                        if ( $MatExist == 1 ) {
                            $NumBulletin = $MatExists['NumBulletin'];
                            $Mtt_emploi = $DB->db->prepare( 'SELECT * FROM bulletin WHERE NumBulletin=:NumBulletin' );
                            $Mtt_emploi->bindParam( ':NumBulletin', $NumBulletin );
                            $Mtt_emploi->execute();
                            $Mtt_emploi = $Mtt_emploi->fetch();
                            $Mtt_emploi = $Mtt_emploi['TauxBulletin'];
                            $heureajouter = ( int )$heureajouter;
                            $heureretranc = ( int )$heureretranc;
                            $NbrH = $heureffec+$heureajouter-$heureretranc;
							$date=date('d-m-Y');
                            $Point = $MatExists['Matricule_emploi'].'_'.$MatExists['nom_emploi'].' '.$MatExists['Prenom_emploi']."_".$date."_".$NbrH."_".$Mtt_emploi.'_'.$MatExists['Matricule_emploi'];
                            if ( !isset( $_SESSION['pointage'] ) ) {
                                $_SESSION['pointage'][] = $Point;
                            } else {
                                if ( !in_array( $Point, $_SESSION['pointage'] ) ) {
                                    $_SESSION['pointage'][] = $Point;
                                } else {
                                    $error = "L'employé a été dejà pointé";
                                }
                            }
                        } else {
                            $error = "Le matricule entrez ne correspond pas à un employé embauché";
                        }
                    } else {
                        $error = "le champ Matricule est vide";
                    }
                } else {
                    $error = "le champ Heure à ajouter est vide";
                }

            } else {
                $error = "le champ Heure effectuée doit etre supérieur";
            }
        } else {
            $error = "le champ Heure effectuée est vide";
        }

    } else {
        $error = "Le champ Nom & Prenom est Vide";
    }
}
?>