<?php
//verification des valeurs saissient dans les inputs
//dectecter si le butons valide a été apuyer
if ( isset( $_POST['Valider'] ) ) {
    //verifie si la variable Matricule existe et non vide
    if ( isset( $_POST['Matricule'] ) && !empty( $_POST['Matricule'] ) ) {
        //verifier que la taille soit supperieur à 5
        if ( strlen( $_POST['Matricule'] ) >= 5 ) {
            //récuperation de la valeur du champ Matricule
            $Matricule = $_POST['Matricule'];
            //verifier si le champ Nom est existe et non vide
            if ( isset( $_POST['Nom'] ) && !empty( $_POST['Nom'] ) ) {
                //verifier la tailles du champ Nom
                if ( strlen( $_POST['Nom'] ) >= 2 ) {
                    //recuperation de la valeur du champ Nom
                    $Nom = $_POST['Nom'];
                    //verifier si la champ prenom existe et non vide
                    if ( isset( $_POST['prenom'] ) && !empty( $_POST['prenom'] ) ) {
                        //verifier la tailles du champ prenom
                        if ( strlen( $_POST['prenom'] ) >= 3 ) {
                            //recuperation de la valeur du champ prenom
                            $prenom = $_POST['prenom'];
                            //verifier si la champ téléphone exites et non vide
                            if ( isset( $_POST['tel'] ) && !empty( $_POST['tel'] ) ) {
                                //verification de la taille du numero du tel
                                if ( strlen( $_POST['tel'] ) == 10 ) {
                                    //verifier si valeur est numeric
                                    if ( is_numeric( $_POST['tel'] ) == 1 ) {
                                        //recuperation de valeur du champ Télephone
                                        $tel = $_POST['tel'];
                                        if ( isset( $_POST['Poste'] ) && !empty( $_POST['Poste'] ) OR $_POST['Poste'] == "Selectionnez un poste" ) {
                                            //verification de longueure de la valeur du Poste
                                            if ( strlen( $_POST['Poste'] ) >= 1 ) {
                                                $poste = $_POST['Poste'];
                                                //verifier si la champ equipe exites et non vide
                                                if ( isset( $_POST['equipe'] ) &&  !empty( $_POST['equipe'] ) ) {
                                                    //recuperation de la valeur du champ equipe
                                                    $equipe = $_POST['equipe'];
                                                    //verifier si la champ equipe exites et non vide
                                                    if ( isset( $_POST['date'] ) && !empty( $_POST['date'] ) ) {
                                                        //recuperation de valuer de la date
                                                        $date_n = $_POST['date'];
                                                        //verification si la valeur du champ Addresse existe et est non vide
                                                        if ( isset( $_POST['Addresse'] ) && !empty( $_POST['Addresse'] ) ) {
                                                            //recuperation de la valeur Addresse
                                                            $Addresse = $_POST['Addresse'];
                                                            //verification de la valeur du champ Catégorie
                                                            if ( isset( $_POST['Categorie'] ) && !empty( $_POST['Categorie'] ) ) {
                                                                //recuperation de la valeur du champ Categorie
                                                                $Categorie = $_POST['Categorie'];
                                                                //verification de la valeur du champ montant
                                                                if ( isset( $_POST['montant'] ) && !empty( $_POST['montant'] ) ) {
                                                                    //verification si la valeur montant est numerique
                                                                    if ( is_numeric( $_POST['montant'] ) == 1 ) {
                                                                        //recuperation de la valeur
                                                                        $montant = $_POST['montant'];
                                                                        if ( isset( $_POST['Contrats'] ) && !empty( $_POST['Contrats'] ) && $_POST['Contrats'] != "Selectionnez un contrat" ) {
                                                                            //recuperation de la valeur  du champ contrat
                                                                            $Contrats = $_POST['Contrats'];
                                                                            // creation du NumBulletin
                                                                            $NumCont = rand( 1000000,  2147483647 );
                                                                            // creation du NumCont
                                                                            $NumBulletin = rand( 106000, 99999 ).rand( 0444, 9999 );
                                                                            // creation de la date
                                                                            $date = date( "Y-m-d" );
                                                                            //appel à la variables de connexion à la base de donneé
                                                                            $NumPoste = rand( 10000000,  2147483647 );
                                                                            //initialisation de la classe config-db
                                                                            $DB = new DB();
                                                                            //enregistrement de l'employé
																			$existmatri=$DB->db->prepare('SELECT * FROM employe WHERE Matricule_emploi = :Matricule_emploi');
																			$existmatri->bindParam('Matricule_emploi',$Matricule);
																			$existmatri->execute();
																			$existmatri=$existmatri->rowCount();
																			if($existmatri==0){
																				//enregistrement employe
																				$ene_employe = $DB->db->prepare( "INSERT INTO employe(Matricule_emploi,nom_emploi,Prenom_emploi,tel_emploi,Num_equip_emploi,DtNs_emploi,Addr_emploi,NumBulletin,Contrats_emploi,NumPoste,NumCont)VALUES(:Matricule_emploi,:nom_emploi,:Prenom_emploi,:tel_emploi,:Num_equip_emploi,:DtNs_emploi,:Addr_emploi,:NumBulletin,:Contrats_emploi,:NumPoste,:NumCont)" );
                                                                            	$ene_employe->bindParam( ':Matricule_emploi', $Matricule );
                                                                            	$ene_employe->bindParam( ':nom_emploi', $Nom );
																				$ene_employe->bindParam( ':Prenom_emploi', $prenom );
																				$ene_employe->bindParam( ':tel_emploi', $tel );
																				$ene_employe->bindParam( ':Num_equip_emploi', $equipe );
																				$ene_employe->bindParam( ':DtNs_emploi', $date_n );
																				$ene_employe->bindParam( ':Addr_emploi', $Addresse );
																				$ene_employe->bindParam( ':NumBulletin', $NumBulletin );
																				$ene_employe->bindParam( ':Contrats_emploi',$Contrats );
																				$ene_employe->bindParam( ':NumPoste',$NumPoste );
																				$ene_employe->bindParam( ':NumCont',$NumCont );
																				$ene_employe->execute();
																				//enregistrement du poste
																				$enr_poste = $DB->db->prepare( 'INSERT INTO poste(NumPoste,LibPoste,DateAff )VALUES(:NumPoste,:LibPoste,:DateAff )');
																				$enr_poste->bindParam( ':NumPoste', $NumPoste);
																				$enr_poste->bindParam( ':LibPoste', $poste );
																				$enr_poste->bindParam( ':DateAff', $date );
																				$enr_poste->execute();
																				//enregistrement du contrat
																				$enregi_cont = $DB->db->prepare( 'INSERT INTO contrat(NumCont,LibCont,DatCont)VALUES(:NumCont,:LibCont,:DatCont)');
																				$enregi_cont->bindParam( ':NumCont', $NumCont );
																				$enregi_cont->bindParam( ':LibCont', $Contrats );
																				$enregi_cont->bindParam( ':DatCont', $date );
																				$enregi_cont->execute();
																				//enregistrement du bulletin
																				$enregi_bull=$DB->db->prepare('INSERT INTO bulletin( NumBulletin, TauxBulletin, DateBulletin )VALUES( :NumBulletin, :TauxBulletin, :DateBulletin )');
																				$enregi_bull->bindParam(':NumBulletin',$NumBulletin);
																				$enregi_bull->bindParam(':TauxBulletin',$montant);
																				$enregi_bull->bindParam(':DateBulletin',$date);
																				$enregi_bull->execute();
																				// enregistrement de la table Gerent
																				if($Categorie=="GERANT"){
																					//
																					$NumGerant=rand( 6000, 99999).rand( 0444,9999);
																					$enregi_gerant=$DB->db->prepare('INSERT INTO gerant (NumGerant,NomGerant,PrenGerant,AdrGerant,TelGerant,NumPoste)VALUES( :NumGerant, :NomGerant, :PrenGerant, :AdrGerant, :TelGerant,:NumPoste)');
																					$enregi_gerant->bindParam(':NumGerant',$NumGerant);
																					$enregi_gerant->bindParam(':NomGerant',$Nom);
																					$enregi_gerant->bindParam(':PrenGerant',$prenom);
																					$enregi_gerant->bindParam(':AdrGerant',$Addresse);
																					$enregi_gerant->bindParam(':TelGerant',$tel);
																					$enregi_gerant->bindParam(':NumPoste',$NumPoste);
																					$enregi_gerant->execute();
																					
																					}elseif($Categorie=="COMPTABLE"){
																						//enregistrement COMPTABLE
																						$NumComp=rand( 6000, 899999).rand( 0444,9999);
																						$enregi_comp=$DB->db->prepare("INSERT INTO comptable(NumComp,NomComp,PrenComp,AdrComp,TelComp)VALUES(:NumComp,:NomComp,:PrenComp,:AdrComp,:TelComp)");
																						$enregi_comp->bindParam(':NumComp',$NumComp);
																						$enregi_comp->bindParam(':NomComp',$Nom);
																						$enregi_comp->bindParam(':PrenComp',$prenom);
																						$enregi_comp->bindParam(':AdrComp',$Addresse);
																						$enregi_comp->bindParam(':TelComp',$tel);
																						$enregi_comp->execute();
																					}
																			}else{
																				$error="Le matricule est déjà utilisé";
																			}
                                                                        } else {
                                                                            $error = "Selectionnez un contrat";
                                                                        }
                                                                    } else {
                                                                        $error = "Entrez une bonne chiffré valeur pour le champ montant";
                                                                    }
                                                                } else {
                                                                    $error = "Entrez une valeur pour le champ montant";
                                                                }
                                                            } else {
                                                                $error = "Entrez une valeur pour le champ Catégorie";
                                                            }
                                                        } else {
                                                            $error = "Entrez une valeur pour le champ Addresse";
                                                        }
                                                    } else {
                                                        $error = "Entrez une date valide";
                                                    }
                                                } else {
                                                    $error = "Entrez une valeur pour le champ téléphone";
                                                }
                                            } else {
                                                $error = "Entrez une bonne valeur pour la Poste";
                                            }
                                        } else {
                                            $error = "Entrez une valeur pour le champ Poste ou <strong>creer un post avant d'embaucher un employé</strong>";
                                        }
                                    } else {
                                        $error = "Numero de Téléphone non valide";
                                    }
                                } else {
                                    $error = "Numero de Téléphone non valide";
                                }
                            } else {
                                $error = "Entrez une valeur pour le champ téléphone";
                            }
                        } else {
                            $error = 'La taille de la valeur du champ prenom est inférieur à 3';
                        }
                    } else {
                        $error = "Entrez une valeur pour le champ prenom";
                    }
                } else {
                    $error = "La taille de la valeur du champ Nom est inférieur à 2";
                }
            } else {
                $error = "Entrez une valeur pour le champ Nom";
            }
        } else {
            $error = "La taille de la valeur du champ Matricule est inférieur à 5";
        }

    } else {
        $error = "Entrez une valeur pour le champ Matricule";
                                                                        }
                                                                    }
                                                                    ?>