<?php
require_once( "../php/config-db.class.php" );
$DB = new DB();

function securite( $donnee ) {
    //On affecte les valeures du resultat des functions dans la variables de departs
    $donnee = trim( $donnee );
    //supprime les caractères invisibles en début et fin de chaîne espaces https://www.php.net/manual/fr/function.trim.php
    $donnee = stripslashes( $donnee );
    //Retourne une chaîne dont les antislashs on été supprimés.
    $donnee = strip_tags( $donnee );
    //supprimé tous les octets nuls, toutes les balises PHP et HTML du code. https://www.php.net/manual/fr/function.strip-tags
    return $donnee;
    // retourne la varaible pourqu'on puisse  la récuperer hors function
}
//appel au fichier pour verification des accèes pour connexion
if ( isset( $_GET['del-poste'] ) ) {
    $num=securite($_GET['del-poste']);
    $del_poste=$DB->db->prepare("DELETE FROM poste WHERE NumPost =:NumPost");
    $del_poste->bindParam(':NumPost', $num );
    $del_poste->execute();
    header('location:../admin/create-postes.php#liste' );
}
?>