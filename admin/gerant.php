<?php 
//appel au ficheir de connexion à la base de donnée
require_once("../php/config-db.class.php");
//appel au fichier pour verification des accèes pour connexion
include('../php/show_bulletin.php');
?>
<!doctype html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
	<meta name="generator" content="Hugo 0.84.0">
	<title>Nouveau employés · SELNIAM</title>

	<link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/dashboard/">



	<!-- Bootstrap core CSS -->
	<link href="../css/bootstrap.min.css" rel="stylesheet">

	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
				font-size: 3.5rem;
			}
		}
	</style>


	<!-- Custom styles for this template -->
	<link href="dashboard.css" rel="stylesheet">
</head>

<body>
	<?php include('../content/header.php'); ?>
	<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
			<h1 class="h2 uppercase">Gestion des Bulletin</h1>
		</div>

		<div class="my-4 w-100 mx-auto">
			<?php 
            //verifier si existe une variable error
            if(isset($error)){?>
			<div class="alert alert-danger alert-dismissible fade show mt-5 mb-3" role="alert">
				<strong>Erreur !</strong> <!-- une autre manière simplifier pour afficher une seule info en php 😉--><?=$error?> .
			</div>
			<?php }?>
			<p>Veuillez entrer les coordonnées d'un employés pour bulletin de solde.</p>
			<div class="container">
				<h2 id="liste">Liste de tout les Bulletins</h2>
				<div class="table-responsive">
					<table class="table table-striped table-sm">
						<thead>
							<tr>
								<th scope="col">id</th>
								<th scope="col">Numero de Bulletin</th>
								<th scope="col">Nom d'employé</th>
								<th scope="col">Prenom d'employé</th>
								<th scope="col">Poste d'employé</th>
								<th scope="col">Type de contrat</th>
								<th scope="col">Voir le bulletin de solde</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=0; foreach($show_bulletin as $show_bulletin){?>
							<tr>
								<td><?=$i?></td>
								<td><?=$show_bulletin->NumBulletin?></td>
								<td><?php  $nom_emploi=NumBulletin($show_bulletin->NumBulletin); echo $nom_emploi->nom_emploi;?></td>
								<td><?php  $nom_emploi=NumBulletin($show_bulletin->NumBulletin); echo $nom_emploi->Prenom_emploi;?></td>
								<td><?php  $post=Poste($show_bulletin->NumBulletin); echo $post->LibPoste;?></td>
								<td></td>
								<td> <a class="nav-link" href="gerant.php?bulletin=<?=$show_bulletin->NumBulletin?>">Consulter</a></td>
							</tr>
							<?php $i+=1; }?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="container">
				<?php if(isset($_GET['bulletin']) && !empty($_GET['bulletin'])){$NumBulletin=$_GET['bulletin']; $show_bulletin_uni=$DB->db->prepare("SELECT * FROM bulletin WHERE NumBulletin=:NumBulletin");
				$show_bulletin_uni->bindParam(':NumBulletin',$NumBulletin);
				$show_bulletin_uni->execute();
				$show_bulletin_uni=$show_bulletin_uni->fetchAll( PDO::FETCH_OBJ );
				?>
				<div class="row">
					<?php foreach($show_bulletin_uni as $show_bulletin_uni){?>
					<h3 id="#Bulletin" class="h3">Bullentin N° : <?=$show_bulletin_uni->NumBulletin?></h3>
					<div class="col-2mt-4">
						<label>Nom : <?php  $nom_emploi=NumBulletin($show_bulletin_uni->NumBulletin); echo $nom_emploi->nom_emploi;?></label>
					</div>
					<div class="col-4 mt-4">
						<label>Prenom : <?php  $nom_emploi=NumBulletin($show_bulletin_uni->NumBulletin); echo $nom_emploi->Prenom_emploi;?></label>
					</div>
					<div class="col-4 mt-4">
						<label>Numero du Contrat : <?php  $Contrat=NumBulletin($show_bulletin_uni->NumBulletin); echo $Contrat->NumCont;?></label>
					</div>
					<div class="col-4 mt-4">
						<label>Poste : <?php  $post=Poste($show_bulletin_uni->NumBulletin); echo $post->LibPoste;?></label>
					</div>
					<div class="col-4 mt-4">
						<label>Type d'employe : <?php  $nom_emploi=NumBulletin($show_bulletin->NumBulletin); echo $nom_emploi->Contrats_emploi;?></label>
					</div>
					<div class="col-4 mt-4">
						<label>Numero de Bulletin : <?=$show_bulletin_uni->NumBulletin?></label>
					</div>
					<div class="col-4 mt-4">
						<label>Date d'embauche : <?=$show_bulletin_uni->DateBulletin?></label>
					</div>
					<div class="col-6 mt-4">
						<label>Taux de travail : <?=$show_bulletin_uni->TauxBulletin?></label>
					</div>
					<div class="col-6 mt-4">
						<label>salaire Journalier : <?php salaire($show_bulletin_uni->NumBulletin);?></label>
					</div>
				</div>
				<?php }}?>
			</div>
		</div>

		<script src="../js/bootstrap.bundle.min.js"></script>

		<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
		<script src="dashboard.js"></script>
</body>

</html>