<?php
//appel au ficheir de connexion à la base de donnée
require_once("../php/config-db.class.php");
//appel au fichier pour verification des accèes pour connexion
include('../php/create-clerk.php');
//affichage du type de contrat pour l'enregistrement d'un employés
include('../php/show-postes.php');
include('../php/show-clerk.php');
?>
<!doctype html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
	<meta name="generator" content="Hugo 0.84.0">
	<title>Nouveau employés · SELNIAM</title>

	<link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/dashboard/">



	<!-- Bootstrap core CSS -->
	<link href="../css/bootstrap.min.css" rel="stylesheet">

	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
				font-size: 3.5rem;
			}
		}
	</style>


	<!-- Custom styles for this template -->
	<link href="dashboard.css" rel="stylesheet">
</head>

<body>
	<?php include('../content/header.php'); ?>
	<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
			<h1 class="h2">ENREGISTREMENT DES EMPLOYES</h1>
		</div>

		<div class="my-4 w-100" width="900" height="380">
			<?php
			//verifier si existe une variable error
			if (isset($error)) { ?>
				<div class="alert alert-danger alert-dismissible fade show mt-5 mb-3" role="alert">
					<strong>Erreur !</strong> <!-- une autre manière simplifier pour afficher une seule info en php 😉--><?= $error ?> .
				</div>
			<?php } ?>
			<div class="alert alert-secondary" role="alert">
				Veuillez creer un poste si besoin pour affecter aux employés
			</div>

			<div class="container">
				<form class="row" action="" method="POST">
					<div class="col-5">
						<div class="mb-3">
							<label for="Matricule" class="form-label">Numero Matricule</label>
							<input type="text" class="form-control" id="Matricule" name="Matricule">
						</div>
						<div class="mb-3">
							<label for="Nom" class="form-label">Nom d'employé </label>
							<input type="text" class="form-control" id="Employe" aria-describedby="Nom" name="Nom">
						</div>
						<div class="mb-3">
							<label for="prenom" class="form-label">Prenom d'employé</label>
							<input type="text" class="form-control" id="prenom" name="prenom">
						</div>
						<div class="mb-3">
							<label for="tel" class="form-label">Téléphone</label>
							<input type="tel" class="form-control" id="tel" name="tel">
						</div>
						<div class="mb-3">
							<label for="Poste" class="form-label">Poste d'employé</label>
							<input type="text" class="form-control" id="Catégorie" aria-describedby="Poste" name="Poste">
						</div>
						<div class="mb-3">
							<label for="equipe" class="form-label">Numero équipe</label>
							<input type="text" class="form-control" id="equipe" name="equipe">
						</div>
					</div>
					<div class="col-5">
						<div class="mb-3">
							<label for="date" class="form-label">Date de naissance</label>
							<input type="date" class="form-control" id="date" name="date">
						</div>
						<div class="mb-3">
							<label for="Addresse" class="form-label">Addresse d'employé</label>
							<input type="text" class="form-control" id="Addresse" name="Addresse">
						</div>
						<div class="mb-3">
							<label for="Categorie" class="form-label">Catégorie d'employé</label>
							<select class="form-select" aria-label="" id='Categorie' name="Categorie">
								<option value="Selectionnez un contrat" selected>Selectionnez un poste</option>
								<?php foreach ($show_postes as $show_poste) : ?>
									<option value="<?= $show_poste->nom_postes ?>"><?= $show_poste->nom_postes ?></option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="mb-3">
							<label for="montant" class="form-label">Montant de paie </label>
							<input type="number" class="form-control" id="montant" aria-describedby="montant" name="montant">
						</div>
						<div class="mb-3">
							<label for="Contrats" class="form-label"> Contrat professionnel</label>
							<select class="form-select" aria-label="" id='Contrats' name="Contrats">
								<option value="Selectionnez un contrat" selected>Selectionnez un contrat</option>
								<option value="Contrat à durée indéterminé(CDI)">Contrat à durée indéterminé(CDI)</option>
								<option value="Contrat à durée déterminé(CDD)">Contrat à durée déterminé(CDD)</option>
							</select>
						</div>
					</div>
					<div class="col-2 mt-4">
						<br>
						<br>
						<br>
						<br>
						<br>
						<div class="mb-3 form-check">
							<button type="submit" class="btn btn-success" style="width: 100%" name="Valider">Valider</button>
						</div>
						<div class="mb-3 form-check">
							<button type="reset" class="btn btn-warning" style="width: 100%">Modifier</button>
						</div>
						<div class="mb-3 form-check">
							<button type="reset" class="btn btn-primary" style="width: 100%">Supprimer</button>
						</div>
						<div class="mb-3 form-check">
							<button type="submit" class="btn btn-danger" style="width: 100%"><a href="index.php" class="nav-link text-white">Quitter</a></button>
						</div>
					</div>
				</form>
			</div>

			<h2>Listes des employés <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people-fill" viewBox="0 0 16 16">
					<path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H7zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
					<path fill-rule="evenodd" d="M5.216 14A2.238 2.238 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.325 6.325 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1h4.216z" />
					<path d="M4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5z" />
				</svg></h2>
			<div class="table-responsive">
				<table class="table table-sm table-hover">
					<thead>
						<tr>
							<th scope="col">Matricule</th>
							<th scope="col">Nom</th>
							<th scope="col">Prénom</th>
							<th scope="col">Téléphone</th>
							<th scope="col">Numero d'équipe</th>
							<th scope="col">Date de naissance</th>
							<th scope="col">Addresse</th>
							<th scope="col">Crontrat professionnel</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($show_clerk as $show_clerk) : ?>
							<tr>
								<td><?= $show_clerk->Matricule_emploi ?></td>
								<td><?= $show_clerk->nom_emploi ?></td>
								<td><?= $show_clerk->Prenom_emploi ?></td>
								<td><?= $show_clerk->tel_emploi ?></td>
								<td><?= $show_clerk->Num_equip_emploi ?></td>
								<td><?= $show_clerk->DtNs_emploi ?></td>
								<td><?= $show_clerk->Addr_emploi ?></td>
								<td><?= $show_clerk->Contrats_emploi ?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
	</main>
	</div>
	</div>


	<script src="../js/bootstrap.bundle.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
	<script src="dashboard.js"></script>
</body>

</html>