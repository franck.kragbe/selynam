<!doctype html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
	<meta name="generator" content="Hugo 0.84.0">
	<title>Tableau de bord · SELNIAM</title>

	<link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/dashboard/">



	<!-- Bootstrap core CSS -->
	<link href="../css/bootstrap.min.css" rel="stylesheet">

	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
				font-size: 3.5rem;
			}
		}
	</style>


	<!-- Custom styles for this template -->
	<link href="dashboard.css" rel="stylesheet">
</head>

<body>
	<?php include('../content/header.php'); ?>
	<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
			<h1 class="h2">Paramètres</h1>
		</div>
		<div class="my-4 w-100" id="myChart" width="900" height="380">
			<div class="row mb-2">
				<div class="col-md-6">
					<div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
						<div class="col p-4 d-flex flex-column position-static">
							<strong class="d-inline-block mb-2 text-primary">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Postes</font>
								</font>
							</strong>
							<h3 class="mb-0">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Affectation d'heures de Travail</font>
								</font>
							</h3>
							<div class="mb-1 text-muted">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Nouveau Poste</font>
								</font>
							</div>
							<p class="card-text mb-auto">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Creer de Nouveaux de postes en un clic tout en tant specifiant les heures correspondantes.</font>
								</font>
							</p>
							<a href="create-postes.php" class="stretched-link nav-link">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">continuer</font>
								</font>
							</a>
						</div>
						<div class="col-auto d-none d-lg-block">
							<svg xmlns="http://www.w3.org/2000/svg" width="200" height="250" fill="currentColor" class="bi bi-person-workspace" viewBox="0 0 16 16">
								<path d="M4 16s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1H4Zm4-5.95a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5Z" />
								<path d="M2 1a2 2 0 0 0-2 2v9.5A1.5 1.5 0 0 0 1.5 14h.653a5.373 5.373 0 0 1 1.066-2H1V3a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v9h-2.219c.554.654.89 1.373 1.066 2h.653a1.5 1.5 0 0 0 1.5-1.5V3a2 2 0 0 0-2-2H2Z" />
							</svg>

						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
						<div class="col p-4 d-flex flex-column position-static">
							<strong class="d-inline-block mb-2 text-primary">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Compte rendu</font>
								</font>
							</strong>
							<h3 class="mb-0">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Rediger des Comptes rendu</font>
								</font>
							</h3>
							<div class="mb-1 text-muted">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Nouveau Compte rendu</font>
								</font>
							</div>
							<p class="card-text mb-auto">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Faite le rapportde toutes la journe</font>
								</font>
							</p>
							<a href="create-compte-rendu.php" class="stretched-link nav-link">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">continuer</font>
								</font>
							</a>
						</div>
						<div class="col-auto d-none d-lg-block">
							<svg xmlns="http://www.w3.org/2000/svg" width="200" height="250" fill="currentColor" class="bi bi-book-fill" viewBox="0 0 16 16">
								<path d="M8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
							</svg>

						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
						<div class="col p-4 d-flex flex-column position-static">
							<strong class="d-inline-block mb-2 text-primary">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Contrats</font>
								</font>
							</strong>
							<h3 class="mb-0">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Voir les contrats</font>
								</font>
							</h3>
							<div class="mb-1 text-muted">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Voir les type d'emplois</font>
								</font>
							</div>
							<p class="card-text mb-auto">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Afficher touts les types de contrats et leurs de creation affecté aux Employées</font>
								</font>
							</p>
							<a href="create-type-contrat.php" class="stretched-link nav-link">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">continuer</font>
								</font>
							</a>
						</div>
						<div class="col-auto d-none d-lg-block">
							<svg xmlns="http://www.w3.org/2000/svg" width="200" height="250" fill="currentColor" class="bi bi-pen-fill" viewBox="0 0 16 16">
								<path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001z" />
							</svg>

						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
						<div class="col p-4 d-flex flex-column position-static">
							<strong class="d-inline-block mb-2 text-primary">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Bulletin</font>
								</font>
							</strong>
							<h3 class="mb-0">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Tout les Bulletins</font>
								</font>
							</h3>
							<div class="mb-1 text-muted">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Voir les Bulletin</font>
								</font>
							</div>
							<p class="card-text mb-auto">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">Afficher la listes de tout les Bulletin des employés</font>
								</font>
							</p>
							<a href="gerant.php" class="stretched-link nav-link">
								<font style="vertical-align: inherit;">
									<font style="vertical-align: inherit;">continuer</font>
								</font>
							</a>
						</div>
						<div class="col-auto d-none d-lg-block">
							<svg xmlns="http://www.w3.org/2000/svg" width="200" height="250" fill="currentColor" class="bi bi-person-lines-fill" viewBox="0 0 16 16">
								<path d="M6 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm-5 6s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H1zM11 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5zm.5 2.5a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1h-4zm2 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2zm0 3a.5.5 0 0 0 0 1h2a.5.5 0 0 0 0-1h-2z" />
							</svg>

						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
	<script src="../js/bootstrap.bundle.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
	<script src="dashboard.js"></script>
</body>

</html>