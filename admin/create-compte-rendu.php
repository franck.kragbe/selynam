<?php 
session_start();
//appel au ficheir de connexion à la base de donnée
require_once("../php/config-db.class.php");
//appel au fichier pour verification des accèes pour connexion
include('../php/create-compte-rendu.php');
?>
<!doctype html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
	<meta name="generator" content="Hugo 0.84.0">
	<title>Nouveau employés · SELNIAM</title>
	<!-- Bootstrap core CSS -->
	<link href="../css/bootstrap.min.css" rel="stylesheet">

	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
				font-size: 3.5rem;
			}
		}

	</style>


	<!-- Custom styles for this template -->
	<link href="dashboard.css" rel="stylesheet">
</head>

<body>
	<?php include('../content/header.php'); ?>
	<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
			<h1 class="h2 uppercase">Compte rendu</h1>
		</div>

		<div class="my-4 w-100 mx-auto">
			<?php 
            //verifier si existe une variable error
            if(isset($error)){?>
			<div class="alert alert-danger alert-dismissible fade show mt-5 mb-3" role="alert">
				<strong>Erreur !</strong> <!-- une autre manière simplifier pour afficher une seule info en php 😉--><?=$error?> .
			</div>
			<?php }?>
			<p>Veuillez entrer les coordonnées des nouveaux employés.</p>
			<div class="container">
				<div class="row">
					<div class="col-6">
						<label for="Numero" class="form-label">Numero d'equipe</label>
						<input type="number" class="form-control" id="Numero" aria-describedby="Numero" disabled name="Numero" value="<?php if(isset($_SESSION["Numero"])){echo $_SESSION["Numero"];} ?>">
					</div>
					<div class="col-6">
						<label for="Numero" class="form-label">Numero du gerant</label>
						<input type="number" class="form-control" id="Numero" aria-describedby="Numero" disabled name="Numero" value="<?php if(isset($_SESSION["NumGerant"])){echo $_SESSION["Numero"];} ?>">
					</div>
					<div class="col-8 mt-2">
						<label for="Chef" class="form-label">Chef d'equipe</label>
						<input type="text" class="form-control" id="Employe" aria-describedby="Chef" disabled name="Chef" value="<?php if(isset($_SESSION["Chef"])){echo $_SESSION["Chef"];} ?>">
					</div>
				</div>
				<div class="col-12">
					<?php   if(isset( $_SESSION['pointage'])){?>
					<h2 id="liste" class="mt-4">Membres d'equipe</h2>
					<table class="table table-striped table-sm text-center">
						<thead>
							<tr>
								<th scope="col">Matricule</th>
								<th scope="col">Nom et Prenom</th>
								<th scope="col">Date</th>
								<th scope="col">Nombres Heures</th>
								<th scope="col">Taux Horaire</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach( $_SESSION['pointage'] as $pointage) :?>
							<?php $pointage=explode('_',$pointage);?>
							<tr>
								<td><?=$pointage[0]?></td>
								<td><?=$pointage[1]?></td>
								<td><?=$pointage[2]?></td>
								<td><?=$pointage[3]?></td>
								<td><?=$pointage[4]?></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<?php } ?>
						<div class="mb-3">
							<button type="submit" onclick="location.href='compt-compte-rendu.php'" class="btn btn-success" style="width: 100%;" name="Valider"> Rendre compte</button>
						</div>
				</div>
			</div>
		</div>
	</main>
	</div>
	</div>


	<script src="../js/bootstrap.bundle.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
	<script src="dashboard.js"></script>
</body>

</html>
