<?php
session_start();
//appel au ficheir de connexion à la base de donnée
require_once("../php/config-db.class.php");
//appel au fichier pour verification des accèes pour connexion
include('../php/compt-compte-rendu.php');
?>
<!doctype html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
	<meta name="generator" content="Hugo 0.84.0">
	<title>Nouveau employés · SELNIAM</title>
	<!-- Bootstrap core CSS -->
	<link href="../css/bootstrap.min.css" rel="stylesheet">

	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			number-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
				font-size: 3.5rem;
			}
		}
	</style>


	<!-- Custom styles for this template -->
	<link href="dashboard.css" rel="stylesheet">
</head>

<body>
	<?php include('../content/header.php'); ?>
	<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
			<h1 class="h2 uppercase">Calcule de Salaires</h1>
		</div>

		<div class="my-4 w-100 mx-auto">
			<?php 
            //verifier si existe une variable error
            if(isset($error)){?>
			<div class="alert alert-danger alert-dismissible fade show mt-5 mb-3" role="alert">
				<strong>Erreur !</strong> <!-- une autre manière simplifier pour afficher une seule info en php 😉--><?=$error?> .
			</div>
			<?php }?>
			<p>Veuillez entrez les informations.</p>
			<div class="container">
				<form class="row" action="" method="POST">
					<div class="col-4 mt-4">
						<label for="heuretravail" class="form-label">Heure de travail</label>
						<input type="number" class="form-control" id="heuretravail" aria-describedby="heuretravail" name="heuretravail" value="<?=$value[3]?>">
					</div>
					<div class="col-4 mt-4">
						<label for="tauxtravail" class="form-label">Taux de travail</label>
						<input type="number" class="form-control" id="tauxtravail" aria-describedby="tauxtravail" value="<?=$value[4]?>" name="tauxtravail" >
					</div>
					<div class="col-4 mt-4">
						<label for="Salaire" class="form-label">Salaire Brute</label>
						<input type="number" class="form-control" id="Salaire" aria-describedby="Salaire" value="<?=$salair?>" name="Salaire">
					</div>
					<div class="col-6 mt-4">
						<label for="Primelog" class="form-label">Prime de logement</label>
						<input type="number" class="form-control" id="Primelog" aria-describedby="Primelog" name="Primelog" required>
					</div>
					<div class="col-6 mt-4">
						<label for="Primetprts" class="form-label">Prime de Transports</label>
						<input type="number" class="form-control" id="Primetprts" aria-describedby="Primetprts" name="Primetprts" required>
					</div>
					<div class="col-12 mt-4">
						<label for="Montantverse" class="form-label">Montant à versé</label>
						<input type="number" class="form-control" id="Montantverse" aria-describedby="Montantverse" name="Montantverse" required>
					</div>
					<div class="col-3 mt-4">
						<p class="btn btn-primary" style="width: 100%" onclick="fair_un_etat()">Faire un etat</p>
					</div>
					<div class="col-3 mt-4">
						<button type="submit" class="btn btn-success" style="width: 100%" name="Valider" value="<?=$key?>">Valider</button>
					</div>
					<div class="col-3 mt-4">
						<button type="reset" class="btn btn-warning" style="width: 100%" name="Valider">Annuler</button>
					</div>
					<div class="col-3 mt-4">
						<button type="button" class="btn btn-danger" style="width: 100%" onclick="location.href='index.php'">Quitter</button>
					</div>
				</form>
			</div>
	</main>
	</div>
	</div>


	<script src="../js/bootstrap.bundle.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
	<script src="../js/compt-calcule.js"></script>
</body>

</html>