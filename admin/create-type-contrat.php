<?php 
//appel au ficheir de connexion à la base de donnée
require_once("../php/config-db.class.php");
//appel au fichier pour 
include('../php/create-type-contrat.php');
?>
<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>Nouveau employés · SELNIAM</title>
    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
</head>

<body>
    <?php include('../content/header.php'); ?>
    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h3 uppercase"> Type de contrat</h1>
        </div>

            <h2>Liste des types de contrats</h2>
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Libélé du contrat</th>
                            <th scope="col">date de creation du type de Contrat</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php foreach($show_contrats as $show_contrat):?>
                        <tr>
                            <td><?=$show_contrat->NumCont?></td>
                            <td><?=$show_contrat->LibCont?></td>
                            <td><?=$show_contrat->DatCont?></td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
    </main>
    </div>
    </div>


    <script src="../js/bootstrap.bundle.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
    <script src="dashboard.js"></script>
</body>

</html>