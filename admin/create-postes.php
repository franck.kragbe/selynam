<?php 
//appel au ficheir de connexion à la base de donnée
require_once("../php/config-db.class.php");
//appel au fichier pour verification des accèes pour connexion
include('../php/create-postes.php');
include('../php/show-postes.php');
include('../php/show-poste.php');
?>
<!doctype html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
	<meta name="generator" content="Hugo 0.84.0">
	<title>Nouveau employés · SELNIAM</title>
	<!-- Bootstrap core CSS -->
	<link href="../css/bootstrap.min.css" rel="stylesheet">

	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
				font-size: 3.5rem;
			}
		}
	</style>


	<!-- Custom styles for this template -->
	<link href="dashboard.css" rel="stylesheet">
</head>

<body>
	<?php include('../content/header.php'); ?>
	<main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
		<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
			<h1 class="h2 uppercase">Affectation d'heures de Travail</h1>
		</div>

		<div class="my-4 w-100 mx-auto">
			<?php 
            //verifier si existe une variable error
            if(isset($error)){?>
			<div class="alert alert-danger alert-dismissible fade show mt-5 mb-3" role="alert">
				<strong>Erreur !</strong> <!-- une autre manière simplifier pour afficher une seule info en php 😉--><?=$error?> .
			</div>
			<?php }?>
			<p>Veuillez entrer les coordonnées du nouveau employé .</p>
			<div class="container">
				<form class="row" action="" method="POST">
					<div class="col-8">
					<div class="mb-3">
							<label for="Matricule" class="form-label">Matricule</label>
							<input type="number" class="form-control" id="Nom" aria-describedby="Matricule" name="Matricule">
						</div>
						<div class="mb-3">
							<label for="HeureDebut" class="form-label">Heure de debut</label>
							<input type="time" class="form-control" id="Employe" aria-describedby="HeureDebut" name="HeureDebut">
						</div>
						<div class="mb-3">
							<label for="HeureFin" class="form-label">Heure de fin</label>
							<input type="time" class="form-control" id="Employe" aria-describedby="HeureFin" name="HeureFin">
						</div>
						<div class="mb-3 form-check">
							<button type="submit" class="btn btn-success" style="width: 100%" name="Valider">Valider</button>
						</div>
					</div>
				</form>
			</div>

			<h2 id="liste">Liste de tout les postes</h2>
			<div class="table-responsive">
				<table class="table table-striped table-sm">
					<thead>
						<tr>
							<th scope="col">Numero de Poste</th>
							<th scope="col">Libéle de Poste</th>
							<th scope="col">Heure de debut</th>
							<th scope="col">Heure de fin</th>
						</tr>
					</thead>
					<tbody>
						<?php include('../php/show-poste.php'); foreach($show_poste as $show_poste){?>
						<tr>
							<td><?=$show_poste->NumPoste ?></td>
							<td><?=$show_poste->LibPoste?></td>
							<td><?=$show_poste->HeureDebut?></td>
							<td><?=$show_poste->HeureFin?></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
	</main>
	</div>
	</div>


	<script src="../js/bootstrap.bundle.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
	<script src="dashboard.js"></script>
</body>

</html>