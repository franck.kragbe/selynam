<?php
//appel au ficheir de connexion à la base de donnée
require_once("php/config-db.class.php");
//appel au fichier pour verification des accèes pour connexion
include('php/conne-user.php');
?>
<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>SELNYAM GES-PAIE</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        * {
            /*changer la police du sites */
            font-family: century gothic;
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
</head>
<!--La Coleure de l'arrier plan en gris-->

<body class="text-center" style="background-color: #a7a7a7">
    <main class="form-signin" style="max-width:500px">
        <form method="POST" action="">
            <img class="mb-4" src="image/logo.png" alt="" width="200" height="auto">
            <h2 class="h2 mb-3 fw-normal  text-white">Bienvenue sur votre Appliction de gestion de paie</h2>
            <h3 class="h3 mb-3 fw-normal  text-white">Veuillez vous connecter</h3>
            <!-- Appel de Php pour afficher des information personalisé du faite de connexion-->
            <!-- Methode pour conbiner le Php au Html-->
            <?php
            //verifier si existe une variable error
            if (isset($error)) { ?>
                <div class="alert alert-warning alert-dismissible fade show mt-5 mb-3" role="alert">
                    <strong>Erreur !&nbsp</strong> <!-- une autre manière simplifier pour afficher une seule info en php 😉--><?= $error ?>
                </div>
            <?php } ?>
            <div class="form-floating">
                <input type="text" class="form-control" id="floatingInput" placeholder="pierre" name="Login">
                <label for="floatingInput">Login</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" id="floatingPassword" placeholder="Password" name="Password">
                <label for="floatingPassword">Mot de passe</label>
            </div>
            <button class="w-100 btn btn-lg text-white" style="background-color:#ffa600" type="submit" name="Connexion">Connexion</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2022–<?= date('Y') ?></p>
        </form>
    </main>
</body>

</html>